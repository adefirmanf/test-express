var express = require('express')
var app = express()
var moment = require('moment')
var index = require('./routes/index.js')

app.use(function(req, res, next){
	console.log(moment().format('YYYY-MM-DD HH:mm:ss'))
	next()
})

app.use('/', index)
app.listen(8001, function(err, results){
	if(err){
		console.log(err)
	}
	console.log("App listening in port : 8001")
})